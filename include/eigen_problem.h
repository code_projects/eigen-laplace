#ifndef __eigen_problem_h
#define __eigen_problem_h

#include <deal.II/grid/tria.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/distributed/tria.h>
#include <deal.II/distributed/solution_transfer.h>

#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/dofs/dof_accessor.h>

#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/mapping_q1.h>

#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/grid/grid_refinement.h>

#include <deal.II/fe/fe_values.h>

#include <deal.II/fe/fe_raviart_thomas.h>

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/timer.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/data_out.h>

#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/precondition.h>

#include <deal.II/base/index_set.h>
#include <deal.II/lac/petsc_sparse_matrix.h>
#include <deal.II/lac/petsc_parallel_vector.h>
#include <deal.II/lac/petsc_parallel_sparse_matrix.h>
#include <deal.II/lac/slepc_solver.h>
#include <deal.II/lac/slepc_spectral_transformation.h>

#include <deal.II/base/mpi.h>

#include <fstream>
#include <iostream>

#include <deal2lkit/parsed_data_out.h>
#include <deal2lkit/parsed_grid_refinement.h>
#include <deal2lkit/parsed_grid_generator.h>
#include <deal2lkit/parsed_function.h>
#include <deal2lkit/error_handler.h>
#include <deal2lkit/utilities.h>
#include <deal2lkit/dof_utilities.h>
#include <deal2lkit/fe_values_cache.h>


using namespace dealii;
using namespace deal2lkit;


template<int dim>
class EigenProblem : public ParameterAcceptor
{
public:

  EigenProblem ();

  virtual void declare_parameters(ParameterHandler &prm);

  void run ();


  template<class Archive>
  void serialize (Archive &ar, const unsigned int);

private:
  void make_grid_fe ();
  void setup_dofs ();
  void setup_system ();
  void assemble_system ();
  void solve ();
  void estimate ();
  void mark_and_refine ();
  void compute_error ();
  void output_results ();
  void create_snapshot();
  void resume_from_snapshot();

  shared_ptr<parallel::distributed::Triangulation<dim> >     triangulation;
  shared_ptr<FiniteElement<dim,dim> > fe;
  shared_ptr<DoFHandler<dim> >        dof_handler;
  shared_ptr<parallel::distributed::SolutionTransfer<dim, PETScWrappers::MPI::Vector> > soltrans;

  IndexSet owned_index_set;
  IndexSet relevant_index_set;

  ConstraintMatrix     constraints;
  PETScWrappers::MPI::SparseMatrix             stiffness_matrix, mass_matrix;
  std::vector<PETScWrappers::MPI::Vector> eigenfunctions;
  std::vector<PETScWrappers::MPI::Vector> eigenfunctions_ghosted;
  std::vector<double>                     eigenvalues;

  PETScWrappers::MPI::Vector system_solution;
  PETScWrappers::MPI::Vector system_solution_ghosted;
  PETScWrappers::MPI::Vector system_rhs;

  Vector<float>         estimated_error_per_cell;

  ParsedDataOut<dim,dim> data_out;
  ParsedGridGenerator<dim,dim> parsed_grid_generator;

  bool source_problem;
  std::string problem_type;
  std::string preconditioner_type;
  double target_eigenvalue;

  unsigned int n_cycles;
  unsigned int cycle;
  unsigned int initial_refinement;
  unsigned int n_eigenvalues;
  unsigned int degree;
  unsigned int precision;
  unsigned int max_iterations;
  double solver_tolerance;
  std::vector<unsigned int> eigenvalue_refinement_indices;
  std::vector<double> exact_eigenvalues;
  std::string estimator_type;
  bool with_cell_terms;

  ParsedGridRefinement pgr;
  ErrorHandler<1> eh;

  ParsedFunction<dim> forcing_laplace;
  ParsedFunction<dim> forcing_maxwell;

  ParsedFunction<dim> exact_laplace;
  ParsedFunction<dim> exact_maxwell;

  std::ofstream timer_outfile;
  ConditionalOStream  pcout;
  TimeMonitor monitor;

  bool resume_computation;
};


#endif
