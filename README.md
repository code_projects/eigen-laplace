# Laplace and Maxwell eigensolvers

**Author**: Luca Heltai <luca.heltai@sissa.it>

This code contains some experiments based on the article

*Optimal convergence of adaptive FEM for eigenvalue clusters in mixed form*
Daniele Boffi, Dietmar Gallistl, Francesca Gardini, Lucia Gastaldi
http://arxiv.org/abs/1504.06418

It solves the Laplace or Maxwell Problems with Raviart-Thomas or Nedelec elements, 
using adaptive mesh refinement based on an estimator constructed using eigenvectors.

## Instruction

You need both the deal.II library (http://www.dealii.org) and the deal.II ToolKit (https://github.com/mathLab/deal2lkit)

	git clone https://gitlab.com/code_projects/eigen-laplace.git
    cd eigen-laplace
    mkdir build
    cd build
    cmake -DDEAL_II_DIR=/path/to/dealii -DD2K_DIR=/path/to/deal2lkit ..
	make

## Input Parameters

When you run the program for the first time, you'll get a file like this in the running directory.

The bulk parameter is called `Top fraction`. If you want to enable also coarsening, then set `Bottom fraction` to a non-zero value.

```bash
subsection Data out
  set Files to save in run directory = 
  set Incremental run prefix         = 
  set Output format                  = vtu
  set Output partitioning            = false
  set Problem base name              = solution
  set Solution names                 = u,u
  set Subdivisions                   = 1
end
subsection Exact solution Laplace
  set Function constants  = 
  set Function expression = sin(2*pi*x)*sin(2*pi*y)
  set Variable names      = x,y,t
end
subsection Exact solution Maxwell
  set Function constants  = 
  set Function expression = 0; 0
  set Variable names      = x,y,t
end
subsection Forcing Laplace
  set Function constants  = 
  set Function expression = 8*pi^2*sin(2*pi*x)*sin(2*pi*y)
  set Variable names      = x,y,t
end
subsection Forcing Maxwell
  set Function constants  = 
  set Function expression = 0; 0
  set Variable names      = x,y,t
end
subsection Global parameters
  set Degree of the finite element space            = 0
  set Eigenvector indices for a posteriori estimate = 1
  set Estimator                                     = boffi
  set Select eigenvalues closest to                 = 6.0
  set Exact eigenvalues                             = 0, 0, 0, 0, 0, 0, 0
  set Initial global refinement                     = 4
  set Number of eigenvalues to compute              = 5
  set Max iterations                                = 10000000
  set Precision of the output                       = 10
  set Preconditioner type                           = cholesky
  set Problem type                                  = maxwell
  set Resume computation                            = false
  set Solve source problem                          = false
  set Solver tolerance                              = 1e-9
  set Total number of cycles                        = 10
  set Use cell residual in estimator                = true
end
subsection deal2lkit::ErrorHandler<1>
  set Compute error            = true
  set Error file format        = txt
  set Error precision          = 11
  set Output error tables      = true
  set Solution names           = E,E
  set Solution names for latex = E,E
  set Table names              = error
  set Write error files        = true
  subsection Table 0
    set Add convergence rates          = false
    set Extra terms                    = cells,dofs
    set Latex table caption            = error
    set List of error norms to compute = Custom; Custom
    set Rate key                       = dofs
  end
end
subsection deal2lkit::ParsedGridGenerator<2, 2>
  set Colorize                      = false
  set Copy boundary to manifold ids = false
  set Copy material to manifold ids = false
  set Create default manifolds      = true
  set Grid to generate              = hyper_L
  set Input grid file name          = ../grids/shifted_sqlr3.msh
  set Manifold descriptors          = 
  set Mesh smoothing alogrithm      = none
  set Optional Point<spacedim> 1    = 0,0
  set Optional Point<spacedim> 2    = 1,1
  set Optional double 1             = 1.0
  set Optional double 2             = -1.0
  set Optional double 3             = 1.5
  set Optional int 1                = 1
  set Optional int 2                = 2
  set Optional vector of dim int    = 1,1
  set Output grid file name         = 
end
subsection deal2lkit::ParsedGridRefinement
  set Bottom fraction                        = 0.000000
  set Maximum number of cells (if available) = 0
  set Order (optimize)                       = 2
  set Refinement strategy                    = fraction
  set Top fraction                           = 0.700000
end
```

See the directory `prms` for example configurations solving a Laplace eigenvalue
problem on a square with a hole, and Maxwell eigenvalue problem on a ell-shaped 
domain.

## Solvers

The Laplace solver uses HYPRE by default, while the Maxwell solver uses
cholesky and MUMPS. 

## Licence

This software is subject to L-GPL version 2.0. See the file LICENCE for
more information.
