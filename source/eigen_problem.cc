/* ---------------------------------------------------------------------
 * Author: Luca Heltai
 * ---------------------------------------------------------------------
 */
#include "eigen_problem.h"
#include <deal.II/base/function.h>
#include <deal.II/fe/fe_nedelec.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/base/function.h>

#include <iomanip>
#include <Teuchos_TimeMonitor.hpp>
#include <deal.II/distributed/solution_transfer.h>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#ifdef DEAL_II_WITH_ZLIB
#  include <zlib.h>
#endif

#include <deal.II/lac/petsc_precondition.h>

#include <petscconf.h>
#include <petscksp.h>
#include <petscpc.h>
#include <slepceps.h>
#include <petscmat.h>
#include <deal.II/lac/slepc_solver.h>

template <int dim>
EigenProblem<dim>::EigenProblem ()
  :
  ParameterAcceptor("Global parameters"),
  data_out("Data out", "vtu"),
  forcing_laplace("Forcing Laplace"),
  forcing_maxwell("Forcing Maxwell", dim),
  exact_laplace("Exact solution Laplace"),
  exact_maxwell("Exact solution Maxwell", dim),
  timer_outfile("timer.txt"),
  pcout(std::cout, Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)==0),
  monitor(MPI_COMM_WORLD, timer_outfile)
{}


template <int dim>
void EigenProblem<dim>::declare_parameters(ParameterHandler &prm)
{

  add_parameter(prm, &initial_refinement, "Initial global refinement", "3",
                Patterns::Integer(0));

  add_parameter(prm, &n_cycles, "Total number of cycles", "6",
                Patterns::Integer(0));

  add_parameter(prm, &precision, "Precision of the output", "6",
                Patterns::Integer(0));

  add_parameter(prm, &max_iterations, "Max iterations", "10000",
                Patterns::Integer(0));

  add_parameter(prm, &solver_tolerance, "Solver tolerance", "1e-9",
                Patterns::Double(0));

  add_parameter(prm, &n_eigenvalues, "Number of eigenvalues to compute", "9",
                Patterns::Integer(0,9));

  add_parameter(prm, &exact_eigenvalues, "Exact eigenvalues",
                "2.0,5.0,5.0,8.0,10.0,10.0,13.0,13.0,17.0,17.0",
                Patterns::List(Patterns::Double()));


  add_parameter(prm, &degree, "Degree of the finite element space", "1",
                Patterns::Integer(0));


  add_parameter(prm, &eigenvalue_refinement_indices, "Eigenvector indices for a posteriori estimate", "3",
                Patterns::List(Patterns::Integer(0),0));

  add_parameter(prm, &estimator_type, "Estimator", "kelly",
                Patterns::Selection("kelly|boffi"));

  add_parameter(prm, &with_cell_terms, "Use cell residual in estimator", "true",
                Patterns::Bool());

  add_parameter(prm, &resume_computation, "Resume computation", "false",
                Patterns::Bool());

  add_parameter(prm, &problem_type, "Problem type", "laplace",
                Patterns::Selection("laplace|maxwell"));

  add_parameter(prm, &preconditioner_type, "Preconditioner type", "cholesky",
                Patterns::Selection("cholesky|hypre"));

  add_parameter(prm, &target_eigenvalue, "Select eigenvalues closest to", "0",
                Patterns::Double());

  add_parameter(prm, &source_problem, "Solve source problem", "false",
                Patterns::Bool());
}

template <int dim>
void EigenProblem<dim>::make_grid_fe ()
{
  {
    auto t = monitor.scoped_timer("Make Grid");
    triangulation = SP(parsed_grid_generator.distributed(MPI_COMM_WORLD));
  }
  if (resume_computation == false)
    {
      triangulation->refine_global(initial_refinement);
    }
  else
    {
      auto t = monitor.scoped_timer("Resume Grid");
      {
        const std::string filename = "restart.mesh";
        std::ifstream in (filename.c_str());
        if (!in)
          AssertThrow (false,
                       ExcMessage (std::string("You are trying to restart a previous computation, "
                                               "but the restart file <")
                                   +
                                   filename
                                   +
                                   "> does not appear to exist!"));
      }
      try
        {
          triangulation->load ("restart.mesh");
        }
      catch (...)
        {
          AssertThrow(false, ExcMessage("Cannot open snapshot mesh file or read the triangulation stored there."));
        }
    }

  dof_handler = SP(new DoFHandler<dim>(*triangulation));
  if (problem_type == "laplace")
    fe=SP(new FE_Q<dim>(std::max((unsigned int)1,degree)));
  else if (problem_type == "maxwell")
    fe=SP(new FE_Nedelec<dim>(degree));
  else
    Assert(false,ExcInternalError("Unrecognized problem type."));

  dof_handler->distribute_dofs(*fe);

  pcout << "Finite element: " << fe->get_name()
        << std::endl;

  pcout << "Number of active cells: "
        << triangulation->n_active_cells()
        << std::endl;

  estimated_error_per_cell.reinit(triangulation->n_active_cells());

}

template <int dim>
void EigenProblem<dim>::setup_dofs ()
{
  auto t = monitor.scoped_timer("Setup Dofs");
  dof_handler->distribute_dofs (*fe);

  pcout << "Number of active cells: "
        << triangulation->n_active_cells()
        << std::endl
        << "Total number of cells: "
        << triangulation->n_cells()
        << std::endl
        << "Number of degrees of freedom: "
        << dof_handler->n_dofs()
        << std::endl;

  DoFTools::extract_locally_owned_dofs  (*dof_handler, owned_index_set);
  DoFTools::extract_locally_relevant_dofs (*dof_handler, relevant_index_set);


  if (source_problem == false)
    {
      eigenfunctions.resize (n_eigenvalues);
      eigenfunctions_ghosted.resize (n_eigenvalues);

      std::vector<PETScWrappers::MPI::Vector *> all_out(n_eigenvalues);
      for (unsigned int i=0; i<n_eigenvalues; ++i)
        {
          eigenfunctions[i].reinit (owned_index_set, MPI_COMM_WORLD);
          all_out[i] = &eigenfunctions[i];
        }

      // Ghosted functions are used internally by soltrans
      if (cycle > 0 && resume_computation == false)
        soltrans->interpolate(all_out);
      else if (resume_computation == true)
        {
          soltrans = SP(new parallel::distributed::SolutionTransfer<dim, PETScWrappers::MPI::Vector>(*dof_handler));
          soltrans->deserialize(all_out);
          resume_computation = false;
        }

      // Now that we have transfered the old ones, reinitialize ghosted vectors.
      for (unsigned int i=0; i<n_eigenvalues; ++i)
        {
          eigenfunctions_ghosted[i].reinit (owned_index_set,
                                            relevant_index_set,
                                            MPI_COMM_WORLD);
          eigenfunctions_ghosted[i] = eigenfunctions[i];
        }

      system_solution_ghosted.reinit(owned_index_set,
                                     relevant_index_set,
                                     MPI_COMM_WORLD);

      eigenvalues.resize (eigenfunctions.size ());

      estimated_error_per_cell.reinit(triangulation->n_active_cells());
    }
  system_solution.reinit(owned_index_set, MPI_COMM_WORLD);
  system_rhs.reinit(owned_index_set, MPI_COMM_WORLD);

}

template <int dim>
void EigenProblem<dim>::setup_system ()
{
  auto t = monitor.scoped_timer("Setup System");

  constraints.clear ();

  DoFTools::make_hanging_node_constraints (*dof_handler, constraints);

  if (problem_type == "laplace")
    VectorTools::interpolate_boundary_values (*dof_handler, 0,
                                              ConstantFunction<dim>(0),
                                              constraints);
  else if (problem_type == "maxwell")
    VectorTools::project_boundary_values_curl_conforming(*dof_handler, 0,
                                                         ConstantFunction<dim>(0.0, dim),
                                                         0, constraints);


  constraints.close ();

  pcout << "Constrained degrees of freedom: "
        << constraints.n_constraints() << std::endl;

  stiffness_matrix.reinit (MPI_COMM_WORLD,
                           dof_handler->n_dofs(),
                           dof_handler->n_dofs(),
                           owned_index_set.n_elements(),
                           owned_index_set.n_elements(),
                           dof_handler->max_couplings_between_dofs());

  mass_matrix.reinit (MPI_COMM_WORLD,
                      dof_handler->n_dofs(),
                      dof_handler->n_dofs(),
                      owned_index_set.n_elements(),
                      owned_index_set.n_elements(),
                      dof_handler->max_couplings_between_dofs());
}

template <int dim>
void EigenProblem<dim>::assemble_system ()
{
  auto t = monitor.scoped_timer("Assemble System");
  QGauss<dim>   quadrature_formula(degree+2);
  QGauss<dim-1> face_quadrature_formula(degree+2);
  FEValues<dim> fe_values (*fe, quadrature_formula,
                           update_values    | update_gradients |
                           update_quadrature_points  | update_JxW_values);
  FEFaceValues<dim> fe_face_values (*fe, face_quadrature_formula,
                                    update_values    | update_normal_vectors |
                                    update_quadrature_points  | update_JxW_values);
  const unsigned int   dofs_per_cell   = fe->dofs_per_cell;
  const unsigned int   n_q_points      = quadrature_formula.size();
  FullMatrix<double>   local_matrix (dofs_per_cell, dofs_per_cell);
  FullMatrix<double>   local_mass_matrix (dofs_per_cell, dofs_per_cell);
  Vector<double>       local_rhs(dofs_per_cell);

  std::vector<double>  forcing_term_laplace(n_q_points);
  std::vector<Vector<double> >  forcing_term_maxwell(n_q_points, Vector<double>(dim));

  std::vector<types::global_dof_index> local_dof_indices (dofs_per_cell);

  const FEValuesExtractors::Scalar pressure (0);
  const FEValuesExtractors::Vector electric_field (0);

  typename DoFHandler<dim>::active_cell_iterator
  cell = dof_handler->begin_active(),
  endc = dof_handler->end();
  for (; cell!=endc; ++cell)
    if (cell->is_locally_owned())
      {
        fe_values.reinit (cell);
        local_matrix = 0;
        local_mass_matrix = 0;
        local_rhs = 0;

        if (problem_type == "laplace")
          {
            forcing_laplace.value_list(fe_values.get_quadrature_points(),
                                       forcing_term_laplace);
            for (unsigned int q=0; q<n_q_points; ++q)
              for (unsigned int i=0; i<dofs_per_cell; ++i)
                {
                  const Tensor<1,dim> grad_phi  = fe_values[pressure].gradient (i, q);
                  const double        phi       = fe_values[pressure].value (i, q);
                  for (unsigned int j=0; j<dofs_per_cell; ++j)
                    {
                      const Tensor<1,dim> grad_u     = fe_values[pressure].gradient (j, q);
                      const double        u          = fe_values[pressure].value (j, q);

                      local_matrix(i,j) += (grad_phi * grad_u)* fe_values.JxW(q);

                      local_mass_matrix(i,j) += (phi*u*fe_values.JxW(q));
                    }
                  local_rhs(i) += phi*forcing_term_laplace[q]*fe_values.JxW(q);
                }
          }
        else if (problem_type == "maxwell")
          {
            forcing_maxwell.vector_value_list(fe_values.get_quadrature_points(),
                                              forcing_term_maxwell);
            for (unsigned int q=0; q<n_q_points; ++q)
              for (unsigned int i=0; i<dofs_per_cell; ++i)
                {
                  const auto curl_phi     = fe_values[electric_field].curl (i, q);
                  const Tensor<1,dim> phi = fe_values[electric_field].value (i, q);
                  for (unsigned int j=0; j<dofs_per_cell; ++j)
                    {
                      const auto curl_u     = fe_values[electric_field].curl (j, q);
                      const Tensor<1,dim> u = fe_values[electric_field].value (j, q);

                      local_matrix(i,j) += (curl_phi * curl_u + (source_problem ? phi *u : 0.0))* fe_values.JxW(q);

                      local_mass_matrix(i,j) += (phi*u*fe_values.JxW(q));
                    }
                  for (unsigned int d=0; d<dim; ++d)
                    local_rhs(i) += phi[d]*forcing_term_maxwell[q][d]*fe_values.JxW(q);
                }
          }


        cell->get_dof_indices (local_dof_indices);

        constraints.distribute_local_to_global(local_matrix,
                                               local_rhs,
                                               local_dof_indices,
                                               stiffness_matrix,
                                               system_rhs);

        constraints.distribute_local_to_global(local_mass_matrix,
                                               local_dof_indices,
                                               mass_matrix);
      }

  stiffness_matrix.compress (VectorOperation::add);
  mass_matrix.compress (VectorOperation::add);
  system_rhs.compress(VectorOperation::add);

// Set all spurious eigenvalues to be 1.234e5
  for (auto i : owned_index_set)
    if (constraints.is_constrained(i))
      {
        stiffness_matrix.set(i, i, 1.234e5);
        mass_matrix.set(i, i, 1.0);
      }

  stiffness_matrix.compress (VectorOperation::insert);
  mass_matrix.compress (VectorOperation::insert);
}

#define SC(i) AssertThrow (i == 0, SLEPcWrappers::SolverBase::ExcSLEPcError(i));

template <int dim>
void EigenProblem<dim>::solve ()
{
  if (source_problem == false)
    {
      auto t = monitor.scoped_timer("Solve");
      auto t2 = monitor.scoped_timer("Solve Cycle "+std::to_string(cycle));

      // Direct SLEPC objects
      EPS eps;
      ST st;
      KSP ksp;
      PC pc;
      PETScWrappers::MPI::SparseMatrix G; // Discrete gradient
      std::vector<PETScWrappers::MPI::Vector> edge_constants(dim);

      PetscInt nconv;
      PetscErrorCode ierr;

      ierr = EPSCreate(MPI_COMM_WORLD, &eps);
      SC(ierr);
      ierr = EPSSetTolerances(eps, solver_tolerance, (PetscInt)max_iterations);
      SC(ierr);
      ierr = EPSSetOperators(eps, stiffness_matrix, mass_matrix);
      SC(ierr);
      ierr = EPSSetProblemType(eps, EPS_GHEP);
      SC(ierr);
      ierr = EPSSetDimensions(eps,(PetscInt) eigenfunctions.size(),
                              PETSC_DECIDE, PETSC_DECIDE);
      SC(ierr);

      ierr = EPSGetST(eps, &st);
      SC(ierr);
      ierr = STGetKSP(st, &ksp);
      SC(ierr);
      ierr = KSPGetPC(ksp, &pc);
      SC(ierr);

      ierr = KSPSetType(ksp, KSPPREONLY);
      SC(ierr);

      ierr = EPSSetWhichEigenpairs(eps, EPS_TARGET_MAGNITUDE);
      SC(ierr);

      ierr = EPSSetTarget(eps, target_eigenvalue);
      SC(ierr);

      AssertThrow(problem_type == "laplace" || target_eigenvalue > 0.0,
                  ExcInternalError("Change your parameter file to search for eigenvalues > 0"));

      if (problem_type == "laplace")
        {
          ierr = EPSSetType(eps, EPSGD);
          SC(ierr);
          ierr = PCSetType(pc, PCHYPRE);
          SC(ierr);
        }
      else if (problem_type == "maxwell")
        {
          ierr = EPSSetType(eps, EPSKRYLOVSCHUR);
          SC(ierr);
          ierr = STSetType(st, STSINVERT);
          SC(ierr);
          if (preconditioner_type == "hypre")
            {
              //   // Preconditioning stuff for hypre ams
              //   const FE_Q<dim> feq1(1);
              //   DoFHandler<dim> dhq1(*triangulation);
              //   dhq1.distribute_dofs(feq1);
              //   IndexSet owned_index_set_q1;
              //   DoFTools::extract_locally_owned_dofs  (dhq1, owned_index_set_q1);

              //   G.reinit(MPI_COMM_WORLD,
              //            dof_handler->n_dofs(),
              //            dhq1.n_dofs(),
              //            owned_index_set.n_elements(),
              //            owned_index_set_q1.n_elements(),
              //            2);

              //   std::vector<unsigned int> local_dofs_q1(feq1.n_dofs_per_cell());
              //   std::vector<unsigned int> local_dofs_edge(fe->n_dofs_per_cell());

              //   typename DoFHandler<dim>::active_cell_iterator
              //   cell = dof_handler->begin_active(),
              //   cellq1 = dhq1.begin_active(),
              //   endc = dof_handler->end();

              //   for (; cell!= endc; ++cell, ++cellq1)
              //     {
              //       Assert(cell->index() == cellq1->index(),
              //              ExcInternalError());
              //       if (cell->is_locally_owned())
              //         {
              //           cellq1->get_dof_indices(local_dofs_q1);
              //           cell->get_dof_indices(local_dofs_edge);
              //           for (unsigned int l=0; l<GeometryInfo<dim>::lines_per_cell; ++l)
              //             {
              //               const unsigned int v0 = GeometryInfo<dim>::line_to_cell_vertices(l, 0);
              //               const unsigned int v1 = GeometryInfo<dim>::line_to_cell_vertices(l, 1);
              //               // Lines coincide with first dofs in edge elements:
              //               G.set(local_dofs_edge[l], local_dofs_q1[v0], -1.0);
              //               G.set(local_dofs_edge[l], local_dofs_q1[v1], 1.0);
              //             }
              //         }
              //     }
              //   G.compress(VectorOperation::insert);

              //   QGauss<dim> quad(2*fe->degree+1);
              //   PETScWrappers::MPI::Vector tmp;
              //   tmp.reinit(owned_index_set, MPI_COMM_WORLD);

              //   KSP ksp2;
              //   PC pc2;
              //   ierr = KSPCreate(MPI_COMM_WORLD,&ksp2);
              //   SC(ierr);

              //   KSPAppendOptionsPrefix(ksp2,"mass_");

              //   ierr = KSPSetOperators(ksp2, mass_matrix, mass_matrix);
              //   SC(ierr);
              //   ierr = KSPSetType(ksp2, KSPCG);
              //   SC(ierr);
              //   ierr = KSPGetPC(ksp2,&pc2);
              //   SC(ierr);

              //   ierr = PCSetType(pc2, PCHYPRE);
              //   SC(ierr);

              //   KSPSetFromOptions(ksp2);

              //   for (unsigned int i=0; i<dim; ++i)
              //     {
              //       edge_constants[i].reinit (owned_index_set, MPI_COMM_WORLD);
              //       ComponentSelectFunction<dim> f(i, 1.0, dim);
              //       VectorTools::create_right_hand_side(*dof_handler,
              //                                           quad, f,
              //                                           tmp,
              //                                           constraints);
              //       tmp.compress(VectorOperation::add);
              //       ierr = KSPSolve(ksp2,tmp,edge_constants[i]);
              //       SC(ierr);
              //       constraints.distribute(edge_constants[i]);
              //     }
              //   KSPDestroy(&ksp2);
              //   SC(ierr);

              //   ierr = PCSetType(pc, "hypre");
              //   SC(ierr);

              //   ierr = PCHYPRESetType(pc, "ams");
              //   SC(ierr);

              //   ierr = PCHYPRESetEdgeConstantVectors(pc, edge_constants[0], edge_constants[1],
              //                                        dim==3 ? edge_constants[2] : NULL);
              //   SC(ierr);

              //   ierr = PCHYPRESetDiscreteGradient(pc, G);
              //   SC(ierr);

            }
          else if (preconditioner_type == "cholesky")
            {
              ierr = PCSetType(pc, "cholesky");
              SC(ierr);

              ierr = PCFactorSetMatSolverPackage(pc, "mumps");
              SC(ierr);
            }
        }
      else
        {
          AssertThrow(false, ExcInternalError());
        }


      ierr = EPSSetFromOptions(eps);
      SC(ierr);
      ierr = EPSSolve(eps);
      SC(ierr);
      ierr = EPSGetConverged(eps, &nconv);
      SC(ierr);

      Assert(nconv >= (PetscInt) eigenfunctions.size(),
             ExcInternalError());

      std::map<double, int> emap;
      for (unsigned int i=0; i<eigenfunctions.size(); ++i)
        {
          double ev;
          EPSGetEigenvalue(eps, i, &ev, PETSC_NULL);
          emap[ev] = i;
        }

      {
        unsigned int i=0;
        for (auto id : emap)
          {
            ierr = EPSGetEigenpair (eps, id.second, &eigenvalues[i], PETSC_NULL,
                                    eigenfunctions[i], PETSC_NULL);
            SC(ierr);
            Assert(id.first == eigenvalues[i], ExcInternalError());
            constraints.distribute (eigenfunctions[i]);
            eigenfunctions[i] /= eigenfunctions[i].linfty_norm ();
            eigenfunctions_ghosted[i] = eigenfunctions[i];
            ++i;
          }
      }
      ierr = EPSDestroy(&eps);
      SC(ierr);
    }
  else
    {
      auto t = monitor.scoped_timer("Solve source problem");
      SolverControl solver_control (max_iterations, solver_tolerance);
      PETScWrappers::SolverCG solver(solver_control, MPI_COMM_WORLD);
      //PETScWrappers::PreconditionBoomerAMG preconditioner(stiffness_matrix);
      PETScWrappers::PreconditionILU preconditioner(stiffness_matrix);

      solver.solve(stiffness_matrix,system_solution, system_rhs, preconditioner);
      constraints.distribute(system_solution);
      system_solution_ghosted = system_solution;
    }
}

#undef SC

inline double square_cross(const Tensor<1,1> &P1,
                           const Tensor<1,1> &P2,
                           const Tensor<1,2> &N,
                           const double &lambda)
{
  auto P3=((P1-P2)/lambda)[0]*N;
  return (P3*P3);
}


inline double square_cross(const Tensor<1,3> &P1,
                           const Tensor<1,3> &P2,
                           const Tensor<1,3> &N,
                           const double &lambda)
{
  auto P3=cross_product_3d((P1-P2)/lambda, N);
  return (P3*P3);
}


template <int dim>
void EigenProblem<dim>::estimate ()
{
  if (source_problem == false)
    {
      auto t = monitor.scoped_timer("Estimate Error");
      if (eigenvalue_refinement_indices.size() == 0)
        return;

      estimated_error_per_cell = 0.0;

      Vector<float> new_estimate(estimated_error_per_cell.size());

      if (estimator_type == "kelly")
        {
          for (unsigned int i=0; i<eigenvalue_refinement_indices.size(); ++i)
            {
              new_estimate = 0.0;
              KellyErrorEstimator<dim>::estimate (*dof_handler,
                                                  QGauss<dim-1>(3),
                                                  typename FunctionMap<dim>::type(),
                                                  eigenfunctions_ghosted[eigenvalue_refinement_indices[i]],
                                                  new_estimate);
              pcout << "Estimator [" << eigenvalue_refinement_indices[i] << "]: "
                    << Utilities::MPI::sum(new_estimate.l2_norm(), MPI_COMM_WORLD)
                    << std::endl;
              estimated_error_per_cell += new_estimate;
            }
        }
      else if (estimator_type == "boffi" && problem_type == "laplace")
        {

          FEValuesCache<dim>  cache   (StaticMappingQ1<dim>::mapping, *fe,
                                       QGauss<dim>(degree+2),
                                       update_values | update_JxW_values | update_hessians,
                                       QGauss<dim-1>(degree+2),
                                       update_normal_vectors | update_gradients | update_JxW_values);


          FEValuesCache<dim>  cache_neighbor   (StaticMappingQ1<dim>::mapping, *fe,
                                                QGauss<dim>(degree+2),
                                                update_default,
                                                QGauss<dim-1>(degree+2),
                                                update_gradients);

          const FEValuesExtractors::Scalar scalar (0);

          for (unsigned int i=0; i<eigenvalue_refinement_indices.size(); ++i)
            {
              new_estimate = 0.0;

              unsigned int id = eigenvalue_refinement_indices[i];
              auto &sol =  eigenfunctions_ghosted[id];
              auto &lambda = eigenvalues[id];

              unsigned int index=0;
              for (auto cell : dof_handler->active_cell_iterators() )
                {
                  if (cell->is_locally_owned())
                    {
                      double local_eta = 0.0;

                      if (with_cell_terms == true)
                        {

                          cache.reinit(cell);
                          cache.cache_local_solution_vector("cell", sol, local_eta);

                          auto &us = cache.get_values("cell", "u", scalar,  local_eta);
                          auto &lapus = cache.get_laplacians("cell", "lapu", scalar,  local_eta);
                          auto &JxW = cache.get_JxW_values();

                          double h_T = cell->diameter();

                          for (unsigned int q=0; q<JxW.size(); ++q)
                            {
                              double r = lapus[q]+lambda*us[q];
                              local_eta += h_T*h_T * (r*r) * JxW[q];
                            }

                          new_estimate[index] += local_eta;
                          local_eta = 0;
                        }

                      for (unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f)
                        {
                          auto face = cell->face(f);
                          // If we don't have cell terms, then use Kelly definition of constant.
                          double h_e = with_cell_terms ? face->diameter()/2.0 : cell->diameter()/24.0;

                          // If the face is on the boundary, there is no estimator
                          if (!face->at_boundary())
                            {
                              // Make sure everything is ok...
                              Assert (cell->neighbor(f).state() == IteratorState::valid, ExcInternalError());

                              auto neighbor = cell->neighbor(f);

                              // The face we are sitting on has children. Our neighbor
                              // is refined, and we are not. Loop over subfaces and
                              // create a subface integral on the neighbor
                              if (face->has_children())
                                {
                                  unsigned int neighbor2=cell->neighbor_face_no(f);
                                  for (unsigned int subface_no=0; subface_no<face->number_of_children(); ++subface_no)
                                    {
                                      local_eta = 0;
                                      auto neighbor_child = cell->neighbor_child_on_subface(f,subface_no);
                                      Assert (!neighbor_child->has_children(), ExcInternalError());

                                      cache_neighbor.reinit(neighbor_child, neighbor2);
                                      cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

                                      auto &ngradus = cache_neighbor.get_gradients("face", "gradu", scalar,  local_eta);

                                      cache.reinit(cell, f, subface_no);
                                      cache.cache_local_solution_vector("face", sol, local_eta);

                                      auto &gradus = cache.get_gradients("face", "gradu", scalar,  local_eta);
                                      auto &JxW = cache.get_JxW_values();
                                      auto &ns = cache.get_current_fe_values().get_all_normal_vectors();

                                      for (unsigned int q=0; q<JxW.size(); ++q)
                                        {
                                          auto dv = (gradus[q]-ngradus[q])*ns[q];
                                          local_eta += h_e*( dv*dv ) *JxW[q];
                                        }
                                      new_estimate[index] += local_eta;
                                    }
                                }
                              else if (!cell->neighbor_is_coarser(f))
                                {
                                  local_eta = 0;
                                  unsigned int neighbor2=cell->neighbor_of_neighbor(f);
                                  cache_neighbor.reinit(neighbor, neighbor2);
                                  cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

                                  auto &ngradus = cache_neighbor.get_gradients("face", "gradu", scalar,  local_eta);

                                  cache.reinit(cell, f);
                                  cache.cache_local_solution_vector("face", sol, local_eta);

                                  auto &gradus = cache.get_gradients("face", "gradu", scalar,  local_eta);
                                  auto &JxW = cache.get_JxW_values();
                                  auto &ns = cache.get_current_fe_values().get_all_normal_vectors();

                                  for (unsigned int q=0; q<JxW.size(); ++q)
                                    {
                                      auto dv = (gradus[q]-ngradus[q])*ns[q];
                                      local_eta += h_e*( dv*dv ) *JxW[q];
                                    }
                                  new_estimate[index] += local_eta;
                                }
                              else
                                {
                                  // Neighbor is coarser than cell

                                  local_eta = 0;
                                  std::pair<unsigned int,unsigned int> neighbor_face_subface
                                    = cell->neighbor_of_coarser_neighbor(f);

                                  Assert (neighbor_face_subface.first<GeometryInfo<dim>::faces_per_cell, ExcInternalError());
                                  Assert (neighbor_face_subface.second<neighbor->face(neighbor_face_subface.first)->number_of_children(),
                                          ExcInternalError());
                                  Assert (neighbor->neighbor_child_on_subface(neighbor_face_subface.first, neighbor_face_subface.second)
                                          == cell, ExcInternalError());

                                  cache_neighbor.reinit(neighbor,
                                                        neighbor_face_subface.first,
                                                        neighbor_face_subface.second);

                                  cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

                                  auto &ngradus = cache_neighbor.get_gradients("face", "gradu", scalar,  local_eta);

                                  cache.reinit(cell, f);
                                  cache.cache_local_solution_vector("face", sol, local_eta);

                                  auto &gradus = cache.get_gradients("face", "gradu", scalar,  local_eta);
                                  auto &JxW = cache.get_JxW_values();
                                  auto &ns = cache.get_current_fe_values().get_all_normal_vectors();

                                  for (unsigned int q=0; q<JxW.size(); ++q)
                                    {
                                      auto dv = (gradus[q]-ngradus[q])*ns[q];
                                      local_eta += h_e*( dv*dv ) *JxW[q];
                                    }
                                  new_estimate[index] += local_eta;
                                }
                            }
                        }
                    }
                  ++index;
                }
              for (auto &e : new_estimate)
                {
                  e = std::sqrt(e);
                }

              pcout << "Estimator [" << id << "]: "
                    << Utilities::MPI::sum(new_estimate.l2_norm(), MPI_COMM_WORLD)
                    << std::endl;

              estimated_error_per_cell += new_estimate;
            }
        }
      else if (estimator_type == "boffi" && problem_type == "maxwell")
        {

          FEValuesCache<dim>  cache   (StaticMappingQ1<dim>::mapping, *fe,
                                       QGauss<dim>(degree+2),
                                       update_values | update_JxW_values | update_hessians,
                                       QGauss<dim-1>(degree+2),
                                       update_normal_vectors | update_gradients | update_JxW_values);


          FEValuesCache<dim>  cache_neighbor   (StaticMappingQ1<dim>::mapping, *fe,
                                                QGauss<dim>(degree+2),
                                                update_default,
                                                QGauss<dim-1>(degree+2),
                                                update_gradients);

          const FEValuesExtractors::Vector electric_field (0);

          for (unsigned int i=0; i<eigenvalue_refinement_indices.size(); ++i)
            {
              new_estimate = 0.0;

              unsigned int id = eigenvalue_refinement_indices[i];
              auto &sol =  eigenfunctions_ghosted[id];
              auto &lambda = eigenvalues[id];

              unsigned int index=0;
              for (auto cell : dof_handler->active_cell_iterators() )
                {
                  if (cell->is_locally_owned())
                    {
                      double local_eta = 0.0;

                      if (with_cell_terms == true)
                        {
                          cache.reinit(cell);
                          cache.cache_local_solution_vector("cell", sol, local_eta);

                          auto &Es = cache.get_values("cell", "u", electric_field,  local_eta);
                          auto &Hs = cache.get_hessians("cell", "hessian", electric_field,  local_eta);
                          auto &divEs = cache.get_divergences("cell", "div", electric_field,  local_eta);
                          auto &JxW = cache.get_JxW_values();


                          double h_T = cell->diameter();

                          for (unsigned int q=0; q<JxW.size(); ++q)
                            {
                              Tensor<1,dim> curl_curl_E;

                              for (unsigned int i=0; i<dim; ++i)
                                for (unsigned int j=0; j<dim; ++j)
                                  curl_curl_E[i] += Hs[q][j][j][i]-Hs[q][i][j][j];

                              auto r = Es[q]-curl_curl_E/lambda;
                              auto &divE = divEs[q];

                              local_eta += h_T*h_T * (r*r + divE*divE) * JxW[q];
                            }

                          new_estimate[index] += local_eta;
                        }

                      for (unsigned int f=0; f<GeometryInfo<dim>::faces_per_cell; ++f)
                        {
                          auto face = cell->face(f);
                          // If we don't have cell terms, then use Kelly definition of constant.
                          double h_e = with_cell_terms ? face->diameter()/2.0 : cell->diameter()/24.0;

                          // If the face is on the boundary, there is no estimator
                          if (!face->at_boundary())
                            {
                              // Make sure everything is ok...
                              Assert (cell->neighbor(f).state() == IteratorState::valid, ExcInternalError());

                              auto neighbor = cell->neighbor(f);

                              // The face we are sitting on has children. Our neighbor
                              // is refined, and we are not. Loop over subfaces and
                              // create a subface integral on the neighbor
                              if (face->has_children())
                                {
                                  unsigned int neighbor2=cell->neighbor_face_no(f);
                                  for (unsigned int subface_no=0; subface_no<face->number_of_children(); ++subface_no)
                                    {
                                      local_eta = 0;
                                      auto neighbor_child = cell->neighbor_child_on_subface(f,subface_no);
                                      Assert (!neighbor_child->has_children(), ExcInternalError());

                                      cache_neighbor.reinit(neighbor_child, neighbor2);
                                      cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

                                      auto &nEs = cache_neighbor.get_values("face", "E", electric_field,  local_eta);
                                      auto &ncurlEs = cache_neighbor.get_curls("face", "curlE", electric_field,  local_eta);

                                      cache.reinit(cell, f, subface_no);
                                      cache.cache_local_solution_vector("face", sol, local_eta);

                                      auto &Es = cache.get_values("face", "E", electric_field,  local_eta);
                                      auto &curlEs = cache.get_curls("face", "curlE", electric_field,  local_eta);

                                      auto &JxW = cache.get_JxW_values();
                                      auto &ns = cache.get_current_fe_values().get_all_normal_vectors();

                                      for (unsigned int q=0; q<JxW.size(); ++q)
                                        {
                                          double dv2 = square_cross(curlEs[q], ncurlEs[q], ns[q], lambda);
                                          double dv = (Es[q]-nEs[q])*ns[q];

                                          local_eta += h_e*( dv2 + dv*dv ) *JxW[q];
                                        }
                                      new_estimate[index] += local_eta;
                                    }
                                }
                              else if (!cell->neighbor_is_coarser(f))
                                {
                                  local_eta = 0;
                                  unsigned int neighbor2=cell->neighbor_of_neighbor(f);
                                  cache_neighbor.reinit(neighbor, neighbor2);
                                  cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

                                  cache.reinit(cell, f);
                                  cache.cache_local_solution_vector("face", sol, local_eta);

                                  auto &nEs = cache_neighbor.get_values("face", "E", electric_field,  local_eta);
                                  auto &ncurlEs = cache_neighbor.get_curls("face", "curlE", electric_field,  local_eta);

                                  auto &Es = cache.get_values("face", "E", electric_field,  local_eta);
                                  auto &curlEs = cache.get_curls("face", "curlE", electric_field,  local_eta);

                                  auto &JxW = cache.get_JxW_values();
                                  auto &ns = cache.get_current_fe_values().get_all_normal_vectors();

                                  for (unsigned int q=0; q<JxW.size(); ++q)
                                    {
                                      double dv2 = square_cross(curlEs[q], ncurlEs[q], ns[q], lambda);
                                      double dv = (Es[q]-nEs[q])*ns[q];

                                      local_eta += h_e*( dv2 + dv*dv ) *JxW[q];
                                    }
                                  new_estimate[index] += local_eta;
                                  local_eta = 0;
                                }
                              else
                                {
                                  // Neighbor is coarser than cell

                                  local_eta = 0;
                                  std::pair<unsigned int,unsigned int> neighbor_face_subface
                                    = cell->neighbor_of_coarser_neighbor(f);

                                  Assert (neighbor_face_subface.first<GeometryInfo<dim>::faces_per_cell, ExcInternalError());
                                  Assert (neighbor_face_subface.second<neighbor->face(neighbor_face_subface.first)->number_of_children(),
                                          ExcInternalError());
                                  Assert (neighbor->neighbor_child_on_subface(neighbor_face_subface.first, neighbor_face_subface.second)
                                          == cell, ExcInternalError());

                                  cache_neighbor.reinit(neighbor,
                                                        neighbor_face_subface.first,
                                                        neighbor_face_subface.second);

                                  cache_neighbor.cache_local_solution_vector("face", sol, local_eta);

                                  cache.reinit(cell, f);
                                  cache.cache_local_solution_vector("face", sol, local_eta);

                                  auto &nEs = cache_neighbor.get_values("face", "E", electric_field,  local_eta);
                                  auto &ncurlEs = cache_neighbor.get_curls("face", "curlE", electric_field,  local_eta);

                                  auto &Es = cache.get_values("face", "E", electric_field,  local_eta);
                                  auto &curlEs = cache.get_curls("face", "curlE", electric_field,  local_eta);

                                  auto &JxW = cache.get_JxW_values();
                                  auto &ns = cache.get_current_fe_values().get_all_normal_vectors();

                                  for (unsigned int q=0; q<JxW.size(); ++q)
                                    {
                                      double dv2 = square_cross(curlEs[q], ncurlEs[q], ns[q], lambda);
                                      double dv = (Es[q]-nEs[q])*ns[q];

                                      local_eta += h_e*( dv2 + dv*dv ) *JxW[q];
                                    }
                                  new_estimate[index] += local_eta;
                                  local_eta = 0;
                                }
                            }
                        }
                    }
                  ++index;
                }
              for (auto &e : new_estimate)
                {
                  e = std::sqrt(e);
                }

              pcout << "Estimator [" << id << "]: "
                    << Utilities::MPI::sum(new_estimate.l2_norm(), MPI_COMM_WORLD)
                    << std::endl;

              estimated_error_per_cell += new_estimate;
            }
        }
      else     // Estimator type.
        {
          Assert(false, ExcInternalError());
        }
    }
}

template <int dim>
void EigenProblem<dim>::mark_and_refine ()
{
  auto t = monitor.scoped_timer("Mark and Refine");
  if (eigenvalue_refinement_indices.size() == 0 || source_problem)
    {
      triangulation->refine_global (1);
    }
  else
    {
      soltrans = SP(new parallel::distributed::SolutionTransfer<dim, PETScWrappers::MPI::Vector>(*dof_handler));
      pgr.mark_cells(estimated_error_per_cell, *triangulation);
      triangulation->prepare_coarsening_and_refinement();
      static std::vector<const PETScWrappers::MPI::Vector *> all_in(n_eigenvalues);
      for (unsigned int i=0; i<eigenfunctions_ghosted.size(); ++i)
        all_in[i] = &eigenfunctions_ghosted[i];
      soltrans->prepare_for_coarsening_and_refinement(all_in);
      triangulation->execute_coarsening_and_refinement ();
    }
}

template <int dim>
void EigenProblem<dim>::output_results ()
{
  auto t = monitor.scoped_timer("Output Solution");
  std::string suff = "_"+Utilities::int_to_string(cycle);

  data_out.prepare_data_output(*dof_handler, suff);
  if (source_problem == false)
    {
      for (unsigned int i=0; i<eigenfunctions.size(); ++i)
        {
          std::string toi=Utilities::int_to_string(i);
          data_out.add_data_vector (eigenfunctions_ghosted[i], "u"+toi);
        }
      data_out.add_data_vector (estimated_error_per_cell, "eta");
    }
  else
    {
      data_out.add_data_vector(system_solution, "u");
    }
  data_out.write_data_and_clear();
  // Write the grid
  std::string outgrid="mesh"+suff+".ar";
  parsed_grid_generator.write(*triangulation, outgrid);
}

template <int dim>
void EigenProblem<dim>::compute_error ()
{
  if (source_problem == false)
    {
      auto t = monitor.scoped_timer("Compute Eigenvalue Errors");
      for (unsigned int i=0;
           i<std::min(exact_eigenvalues.size(), eigenvalues.size());
           ++i)
        {
          eh.custom_error([this, i] (const unsigned int)
          {
            return std::abs(eigenvalues[i]-exact_eigenvalues[i]);
          }, *dof_handler, "l"+Utilities::int_to_string(i), i==0);
//     eh.custom_error([this, i] (const unsigned int) {
//  return eigenvalues[i];
//       }, *dof_handler, "l"+Utilities::int_to_string(i), i==0, 1);
        }
    }
  else
    {
      auto t = monitor.scoped_timer("Compute Source Errors");
      if (problem_type == "laplace")
        eh.error_from_exact(*dof_handler,system_solution_ghosted,exact_laplace);
      else if (problem_type == "maxwell")
        eh.error_from_exact(*dof_handler,system_solution_ghosted,exact_maxwell);
    }
}


template <int dim>
void EigenProblem<dim>::run ()
{
  pcout << std::setprecision(precision);
  {
    auto t = monitor.scoped_timer("Run");

    if (source_problem == true)
      resume_computation = false;

    make_grid_fe ();

    // Restore all needed things from the snapshot
    if (resume_computation == true)
      {
        resume_from_snapshot();
        // We need to increase cycle, because otherwise the increase would happen only
        // after the snapshot is created. But if we restore the snapshot, the
        // first ++cycle instruction of the for cycle would happen at the end
        // of the cycle, leaving us with one cycle less
        ++cycle;
        setup_dofs();
      }
    else
      cycle = 0;

    for (; cycle<n_cycles; ++cycle)
      {
        auto t = monitor.scoped_timer("Cycle "+std::to_string(cycle));
        pcout<<"cycle : "<<cycle+1<<std::endl;

        if (cycle > 0)
          {
            estimate ();
            mark_and_refine ();
          }

        setup_dofs();
        setup_system ();
        assemble_system ();
        solve ();
        compute_error ();
        output_results ();
        eh.output_table(pcout);

        // Now dump everything, before starting the new cycle
        if (source_problem == false)
          create_snapshot();
      }

    {
      auto t = monitor.scoped_timer("Output table");
      eh.output_table(pcout);
    }
  }
  //  eh.output_table(pcout,1);
}

// Check point restarts

template <int dim>
void EigenProblem<dim>::create_snapshot()
{
  auto t = monitor.scoped_timer("Create snapshot");

  unsigned int my_id = Utilities::MPI::this_mpi_process (MPI_COMM_WORLD);

  if (my_id == 0)
    {
      // if we have previously written a snapshot, then keep the last
      // snapshot in case this one fails to save. Note: static variables
      // will only be initialied once per model run.
      static bool previous_snapshot_exists = (resume_computation == true);

      if (previous_snapshot_exists == true)
        {
          rename_file ("restart.mesh",
                       "restart.mesh.old");
          rename_file ("restart.mesh.info",
                       "restart.mesh.info.old");
          rename_file ("restart.resume.z",
                       "restart.resume.z.old");
        }
      // from now on, we know that if we get into this
      // function again, a snapshot has previously
      // been written
      previous_snapshot_exists = true;
    }

  // save Triangulation and Solution vectors:
  {
    std::vector<const PETScWrappers::MPI::Vector *> vectors;
    for (auto &e : eigenfunctions_ghosted)
      vectors.push_back(&e);

    soltrans = SP(new parallel::distributed::SolutionTransfer<dim, PETScWrappers::MPI::Vector>(*dof_handler));
    soltrans->prepare_serialization (vectors);

    triangulation->save ("restart.mesh");
  }

  // save general information This calls the serialization functions on all
  // processes (so that they can take additional action, if necessary, see
  // the manual) but only writes to the restart file on process 0
  {
    std::ostringstream oss;

    // serialize into a stringstream
    boost::archive::binary_oarchive oa (oss);
    oa << (*this);

    // compress with zlib and write to file on the root processor
#ifdef DEAL_II_WITH_ZLIB
    if (my_id == 0)
      {
        uLongf compressed_data_length = compressBound (oss.str().length());
        std::vector<char *> compressed_data (compressed_data_length);
        int err = compress2 ((Bytef *) &compressed_data[0],
                             &compressed_data_length,
                             (const Bytef *) oss.str().data(),
                             oss.str().length(),
                             Z_BEST_COMPRESSION);
        (void)err;
        Assert (err == Z_OK, ExcInternalError());

        // build compression header
        const uint32_t compression_header[4]
          = { 1,                                   /* number of blocks */
              (uint32_t)oss.str().length(), /* size of block */
              (uint32_t)oss.str().length(), /* size of last block */
              (uint32_t)compressed_data_length
            }; /* list of compressed sizes of blocks */

        std::ofstream f ("restart.resume.z");
        f.write((const char *)compression_header, 4 * sizeof(compression_header[0]));
        f.write((char *)&compressed_data[0], compressed_data_length);
      }
#else
    AssertThrow (false,
                 ExcMessage ("You need to have deal.II configured with the 'libz' "
                             "option to support checkpoint/restart, but deal.II "
                             "did not detect its presence when you called 'cmake'."));
#endif

  }
  pcout << "*** Snapshot created!" << std::endl << std::endl;
}



template <int dim>
void EigenProblem<dim>::resume_from_snapshot()
{
  {
    const std::string filename = "restart.resume.z";
    std::ifstream in (filename.c_str());
    if (!in)
      AssertThrow (false,
                   ExcMessage (std::string("You are trying to restart a previous computation, "
                                           "but the restart file <")
                               +
                               filename
                               +
                               "> does not appear to exist!"));
  }

  // read zlib compressed resume.z
  try
    {
#ifdef DEAL_II_WITH_ZLIB
      std::ifstream ifs ("restart.resume.z");
      AssertThrow(ifs.is_open(),
                  ExcMessage("Cannot open snapshot resume file."));

      uint32_t compression_header[4];
      ifs.read((char *)compression_header, 4 * sizeof(compression_header[0]));
      Assert(compression_header[0]==1, ExcInternalError());

      std::vector<char> compressed(compression_header[3]);
      std::vector<char> uncompressed(compression_header[1]);
      ifs.read(&compressed[0],compression_header[3]);
      uLongf uncompressed_size = compression_header[1];

      const int err = uncompress((Bytef *)&uncompressed[0], &uncompressed_size,
                                 (Bytef *)&compressed[0], compression_header[3]);
      AssertThrow (err == Z_OK,
                   ExcMessage (std::string("Uncompressing the data buffer resulted in an error with code <")
                               +
                               Utilities::int_to_string(err)));

      {
        std::istringstream ss;
        ss.str(std::string (&uncompressed[0], uncompressed_size));
        boost::archive::binary_iarchive ia (ss);
        ia >> (*this);
      }
#else
      AssertThrow (false,
                   ExcMessage ("You need to have deal.II configured with the 'libz' "
                               "option to support checkpoint/restart, but deal.II "
                               "did not detect its presence when you called 'cmake'."));
#endif
    }
  catch (std::exception &e)
    {
      AssertThrow (false,
                   ExcMessage (std::string("Cannot seem to deserialize the data previously stored!\n")
                               +
                               "Some part of the machinery generated an exception that says <"
                               +
                               e.what()
                               +
                               ">"));
    }
}

//why do we need this?!
BOOST_CLASS_TRACKING (EigenProblem<2>, boost::serialization::track_never)
BOOST_CLASS_TRACKING (EigenProblem<3>, boost::serialization::track_never)

template <int dim>
template<class Archive>
void EigenProblem<dim>::serialize (Archive &ar, const unsigned int)
{
  ar &eigenvalues;
  ar &cycle;
  ar &eh;
}
template class EigenProblem<2>;

